%include "lib.inc"

section .text
global find_word

find_word:
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi
.loop:
	mov rdi, r12
	lea rsi, [r13+8]
	call string_equals
	test rax, rax
	jnz .exit
	mov r13, [r13]
	test r13, r13
	jnz .loop
.exit:
	mov rax, r13
	pop r13
	pop r12
	ret