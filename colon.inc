%define DICTIONARY 0

%macro colon 2
	%ifid %2
		%ifstr %1
			%2:
				dq DICTIONARY
				db %1, 0
			%define DICTIONARY %2
		%endif
    %endif
%endmacro