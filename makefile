ASM=nasm
ASMFLAGS=-felf64
LD=ld
PY = python3
PROGRAM = program
RM = rm -f

$(PROGRAM): main.o lib.o dict.o
	$(LD) -o $@ $^

.PHONY: clear test

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.inc

main.o: main.asm lib.inc dict.inc

clear:
	$(RM) *.o $(PROGRAM);
test: $(PROGRAM)
	$(PY) test.py
