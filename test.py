import subprocess

not_found_message = "key not found"
too_long_message = "the line did not fit into the buffer"
buffer_size = 256

cases = {
	"first": ("firstvalue",''),
	"second": ("secondvalue",''),
	"third": ("thirdvalue",''),
	"nonexistent": ('',not_found_message),
	"0"*(buffer_size-1): ('',not_found_message),
	"0"*buffer_size: ('',too_long_message),
}


scheme = ''
failures = 0
for index, (line, expected) in enumerate(cases.items()):
	process = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	result = process.communicate(line.encode())
	out, err = decoded_result = tuple(map(lambda x: x.decode().strip(), result))
	if decoded_result==expected:
		scheme += '.'
		print("Тест {0} успешно пройден".format(index))
	else:
		failures+=1
		scheme += 'F'
		if err == expected[1]:
			print('Тест {0} не пройден:'.format(index))
			print('Ожидалось - {0}, результат - {1}'.format(expected[0],out))
		else:
			print('Тест {0} не пройден: {1}'.format(index,err))
print(scheme, ' - {0}/{1}'.format(len(cases)-failures,len(cases)))
