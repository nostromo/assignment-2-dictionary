%include "words.inc"
%include "dict.inc"
%include "lib.inc"
%define BUFFER_SIZE 256
%define STDOUT 1
%define STDERR 2

section .rodata
not_found_message: db "key not found", 0
too_long_message: db "the line did not fit into the buffer", 0

section .bss
buffer: resb BUFFER_SIZE

section .text
global _start

_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_word
	test rax, rax
	jz .too_long
	mov rdi, rax
	mov rsi, DICTIONARY
	call find_word
	test rax, rax
	jz .not_found
	push r12
	add rax, 8
	mov r12, rax
	mov rdi, rax
	call string_length
	lea rdi, [r12+rax+1]
	pop r12
	print_and_exit STDOUT, 0
.not_found:
	print_and_exit not_found_message, STDERR, 1
.too_long:
	print_and_exit too_long_message, STDERR, 1
