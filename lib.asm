section .text 
 
%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1

global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%macro print_and_exit 2-3
    %if %0 = 3
        mov rdi, %1
        %rotate 1
    %endif
    mov rsi, %1
    call print_string
    mov rdi, %2
    jmp exit
%endmacro

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    push rsi
    call string_length
    pop rdi
    pop rsi
    mov rdx, rax
    mov rax, WRITE_SYSCALL
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jnl print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, rsp
    sub rsp, 24
    dec rdi
    mov [rdi], byte 0
    mov r9, 10
.division:
    xor rdx, rdx
    div r9
    add dl, '0'
    dec rdi
    mov [rdi], dl
    test rax, rax
    jne .division
    call print_string
    add rsp, 24
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
.loop:
    mov r9b, byte[rdi+rcx]
    cmp r9b, byte[rsi+rcx]
    jne .break
    inc rcx
    test r9b, r9b
    jne .loop
    mov rax, 1
.break:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    dec rsp
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    je .end
    mov al, [rsp]
.end:
    inc rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    dec r13
.spaces:
    call read_char
    cmp rax, ' '
    je .spaces
    cmp rax, `\t`
    je .spaces
    cmp rax, `\n`
    je .spaces
    xor r14, r14
.symbols:
    cmp r13, r14
    jl .failed
    test rax, rax
    je .end
    cmp r13, r14
    je .failed
    cmp rax, ' '
    je .end
    cmp rax, `\t`
    je .end
    cmp rax, `\n`
    je .end
    mov byte[r12+r14], al
    inc r14
    call read_char
    jmp .symbols
.end:
    mov byte[r12+r14], 0
    mov rax, r12
    mov rdx, r14
    jmp .exit
.failed:
    xor rax, rax
.exit:
    pop r14
    pop r13
    pop r12
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    mov r10, 10
.loop:
    mov r9b, byte[rdi+rdx]
    sub r9, '0'
    jl .exit
    cmp r9, 9
    jg .exit
    inc rdx
    push rdx
    mul r10
    pop rdx
    add rax, r9
    jmp .loop
.exit:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r9b, byte[rdi]
    cmp r9, '+'
    je .signed
    cmp r9, '-'
    jne parse_uint
.signed:
    inc rdi
    push r9
    call parse_uint
    pop r9
    inc rdx
    cmp r9, '+'
    je .exit
    neg rax
.exit:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax
    jg .fits
    xor rax, rax
    ret
.fits:
    xor rcx, rcx
.loop:
    cmp rcx, rax
    jg .exit
    mov r9b, byte[rdi+rcx]
    mov byte[rsi+rcx], r9b
    inc rcx
    jmp .loop
.exit:
    ret
